FROM node:17-alpine

EXPOSE 3000

RUN npm i npm@latest -g

COPY package.json package-lock.json ./

RUN npm install

COPY . .

CMD ["node", "src/index.js"]