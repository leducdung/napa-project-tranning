import User from './packages/user/model.js';
import UserAccesses from './packages/userAccesses/model.js';
import Otps from './packages/otp/model.js';

export {
  User,
  UserAccesses,
  Otps,
};
