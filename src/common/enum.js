const UserStatus = {
  inactive: 'inactive',
  active: 'active',
};

const UserAccessStatus = {
  accepted: 'accepted',
  rejected: 'rejected',
  pending: 'pending',
};

const roleName = {
  admin: 'admin',
  basic: 'basic',
};

const rolesList = ['admin', 'basic'];
export {
  UserStatus,
  roleName,
  UserAccessStatus,
  rolesList,
};
