import { validate } from 'express-validation';
import user from '../packages/user/validator.js';
import otp from '../packages/otp/validator.js';

function parse(object) {
  const data = {};
  for (const key of Object.keys(object)) {
    data[key] = validate(object[key], {}, {})
  }
  return data;
}

export default {
  user: parse(user),
  otp: parse(otp),
};
