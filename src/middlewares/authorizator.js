import { rolesList } from '../common/enum.js';
function checkRoles(role) {
  return (req, res, next) => {
    const userRoles = req.userPayload.roleNames || [];
    if (role?.length === 0) {
      role = rolesList;
    }

    const hasRoles = userRoles.some((userRole) => role.includes(userRole));
    if (!hasRoles) {
      return res.status(403).json({
        code: 403,
        message: 'forbidden',
      });
    }

    next();
  };
}

export default {
  checkRoles,
};
