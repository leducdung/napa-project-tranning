import express from 'express';
import passport from 'passport';
import userService from '../../packages/user/service.js';
import to from '../../utils/to.js';
import configEnv from '../../env/index.js';
import github from './github.js';

export const router = express.Router();
router.get(
  '/google',
  passport.authenticate('google', {
    scope: ['profile', 'email'],
  }),
);

router.get(
  '/google/callback',
  passport.authenticate('google', { failureRedirect: '/failed' }),
  async (req, res) => {
    const [error, result] = await to(userService.googleLogin(req.user));

    if (error) {
      return res.redirect(`${configEnv.host.beHost}/users/error/inactiveUser`);
    }
    return res.redirect(`${configEnv.host.feHost}?token=${result}`);
  },
);

const isLoggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.sendStatus(401);
  }
};

router.get('/success', isLoggedIn, (req, res) => res.send(`Welcome mr ${req.user.displayName}!`));

router.get('/github', (req, res) => {
  res.redirect(`https://github.com/login/oauth/authorize?client_id=${configEnv.github.clientID}`);
});

router.get('/github/callback', async (req, res) => {
  const body = {
    clientID: configEnv.github.clientID,
    clientSecret: configEnv.github.clientSecret,
    code: req.query.code,
  };
  const accessToken = await github.getAccessToken(body.code, body.clientID, body.clientSecret);

  const userGithub = await github.fetchGitHubUser(accessToken);

  const [error, token] = await to(userService.gitHubLogin(userGithub));

  if (error) {
    return res.redirect(`${configEnv.host.beHost}/users/error/inactiveUser`);
  }
  return res.redirect(`${configEnv.host.feHost}?token=${token}`);
});

export default router;
