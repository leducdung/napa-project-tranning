import axios from 'axios';

const getAccessToken = async (code, clientId, clientSecret) => {
  try {
    const body = {
      client_id: clientId,
      client_secret: clientSecret,
      code,
    };
    const opts = { headers: { accept: 'application/json' } };
    const response = await axios.post('https://github.com/login/oauth/access_token', body, opts);

    if (!response?.data?.access_token) {
      return false;
    }

    return response?.data?.access_token;
  } catch (error) {
    throw new Error(error);
  }
};

const fetchGitHubUser = async (token) => {
  try {
    const data = await axios.get('https://api.github.com/user', {
      headers: {
        Authorization: `token ${token}`,
      },
    });
    return data;
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  getAccessToken,
  fetchGitHubUser,
};
