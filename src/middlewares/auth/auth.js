import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth20';
import configEnv from '../../env';

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

passport.use(new GoogleStrategy(
  {
    clientID: configEnv.google.clientID,
    clientSecret: configEnv.google.clientSecret,
    callbackURL: configEnv.google.callbackURL,
  },
  ((accessToken, refreshToken, profile, done) =>
    // eslint-disable-next-line implicit-arrow-linebreak
    done(null, profile)
  ),
));
