import jwt from 'jsonwebtoken';
import userService from '../packages/user/service.js';
import userAccessesService from '../packages/userAccesses/service.js';
import to from '../utils/to.js';
import { roleName, UserStatus } from '../common/enum.js';
import configEnv from '../env/index.js';

const authenticateToken = async (req, res, next) => {
  let userPayload;
  let roleNames = [];
  let firstName;
  let lastName;
  let isAdmin = false;
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) {
    return res.status(401).json({
      code: 401,
      message: 'Unauthorized',
    });
  }
  let jwtErr = false;
  jwt.verify(token, configEnv.jwt.secret, (err, user) => {
    if (err) {
      jwtErr = true;
    }
    userPayload = {
      ...userPayload,
      ...user,
    };
  });

  if (!userPayload?.userId || jwtErr) {
    return res.status(403).json({
      code: 403,
      message: 'Forbidden',
    });
  }

  const [[, user], [, userAccesses]] = await Promise.all([
    to(userService.findOne(
      {
        query: {
          _id: userPayload?.userId,
        },
        needToCheckExist: true,
        needToValidateAccess: true,
        credentials: {
          isAdmin: true,
        },
      },
    )),
    to(userAccessesService.getUserAccesses(
      {
        query: {
          userId: userPayload?.userId,
        },
      },
    )),
  ]);

  if (!user || !userAccesses?.list?.length) {
    return res.status(401).json({
      code: 401,
      message: 'required login',
    });
  }

  if (user.status === UserStatus.inactive) {
    return res.status(403).json({
      code: 403,
      message: 'Inactive user',
    });
  }

  firstName = user?.firstName;
  lastName = user?.lastName;
  roleNames = userAccesses?.list?.map((userAccess) => userAccess.roleName);
  isAdmin = !!roleNames.includes(roleName.admin);

  req.userPayload = {
    ...userPayload,
    roleNames,
    firstName,
    lastName,
    isAdmin,
  };

  next();
};

export default {
  authenticateToken,
};
