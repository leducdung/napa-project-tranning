import { Router } from 'express';
import userRoute from '../packages/user/route.js';
import accessesRoute from '../packages/userAccesses/route.js';
import authRoute from '../middlewares/auth/route.js';
import otpRoute from '../packages/otp/route.js';

export default () => {
  const api = Router();
  api.use('/users', userRoute);
  api.use('/accesses', accessesRoute);
  api.use('/auth', authRoute);
  api.use('/otp', otpRoute);
  /**
   * @swagger
   * /getUser:
   *   get:
   *     description: Get all books
   *     responses:
   *       200:
   *         description: Success
   */
  return api;
};
