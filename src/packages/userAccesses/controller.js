import to from '../../utils/to.js';
import service from './service.js';

const createOne = async (req, res) => {
  try {
    const [error, data] = await to(service.createOneAccess(req.body));

    res.send({ error, data });
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  createOne,
};
