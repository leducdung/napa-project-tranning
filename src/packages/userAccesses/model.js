import { mongoose, Schema } from '../../utils/mongoose.js';
import { roleName, UserAccessStatus } from '../../common/enum.js';

const UserAccessesSchema = new Schema(
  {
    userId: { type: Schema.Types.ObjectId, ref: 'Users' },
    roleName: {
      type: String,
      enum: Object.values(roleName),
      require: true,
      default: roleName.basic,
    },
    status: {
      type: String,
      enum: Object.values(UserAccessStatus),
      default: UserAccessStatus.pending,
    },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);

export default mongoose.model('UserAccesses', UserAccessesSchema);
