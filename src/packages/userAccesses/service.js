import { UserAccesses } from '../../models.js';
import { buildFindingQuery } from '../../utils/build.js';
import { validateAccessToList } from '../../utils/validateAccesses.js';

const getUserAccesses = async ({ query }) => {
  try {
    const options = buildFindingQuery(query);
    const {
      limit, skip, sort, search,
    } = options;
    const where = search;

    const [userAccesses, total] = await Promise.all([
      UserAccesses.find(where)
        .sort(sort)
        .skip(skip)
        .limit(limit)
        .populate('userId'),
      UserAccesses.countDocuments(where),
    ]);

    return {
      list: userAccesses,
      pagination: {
        total,
        limit,
        sort,
      },
    };
  } catch (err) {
    throw new Error(err.message);
  }
};

const createOneAccess = async (data) => {
  try {
    const user = await UserAccesses.create(data);
    return user;
  } catch (err) {
    throw new Error(err.message);
  }
};

const deleteMany = async ({ query, credentials }) => {
  try {
    const userAccesses = await UserAccesses.find(query).exec();

    if (!userAccesses?.length) {
      return true;
    }

    const { validDataLength } = validateAccessToList({
      data: userAccesses,
      credentials,
    });

    if (validDataLength !== userAccesses.length) {
      throw new Error(JSON.stringify({
        code: 403,
        message: 'ValidationError',
      }));
    }

    await UserAccesses.deleteMany(query).exec();

    return true;
  } catch (error) {
    return Promise.reject(error);
  }
};

export default {
  getUserAccesses,
  createOneAccess,
  deleteMany,
};
