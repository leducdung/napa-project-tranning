import express from 'express';
import controller from './controller.js';

export const router = express.Router();
router.post('/', controller.createOne);

export default router;
