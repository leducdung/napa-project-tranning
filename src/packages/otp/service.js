import userService from '../user/service.js';
import { Otps } from '../../models.js';
import { buildFindingQuery } from '../../utils/build.js';
import mailService from '../sendMail/service.js';
import { validateAccessToList } from '../../utils/validateAccesses.js';

const getOtps = async ({ query }) => {
  try {
    const options = buildFindingQuery(query);
    const {
      limit, skip, sort, search,
    } = options;
    const where = search;

    const [otps, total] = await Promise.all([
      Otps.find(where)
        .sort(sort)
        .skip(skip)
        .limit(limit)
        .populate('userId'),
      Otps.countDocuments(where),
    ]);

    return {
      list: otps,
      pagination: {
        total,
        limit,
        sort,
      },
    };
  } catch (err) {
    throw new Error(err.message);
  }
};

const createOneOtp = async (data) => {
  try {
    const user = await userService.findOne({
      query: {
        email: data.email,
      },
      needToCheckExist: false,
      needToValidateAccess: false,
      credentials: {
        isAdmin: true,
      },
    });

    if (!user) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'Email not found',
      }));
    }

    const userOpt = await Otps.findOne({
      userId: user._id,
    }).exec();

    const ramdomOtp = Math.random().toString(36).substring(7);
    const expirationTime = new Date().getTime() + 1000 * 60 * 5;
    const createData = {
      otp: ramdomOtp,
      userId: user._id,
      expirationTime: new Date(expirationTime).toISOString(),
    };

    const otpEmailData = mailService.getOTPEmailData({
      to: data.email,
      subject: 'reset passwpord',
      text: 'reset passwpord',
      typeEmail: 'reset passwpord',
      otp: ramdomOtp,
    });

    await mailService.sendEmail(otpEmailData);

    if (userOpt) {
      Object.assign(userOpt, createData);
      userOpt.save();

      return true;
    }

    await new Otps(createData).save();
    return true;
  } catch (err) {
    throw new Error(err.message);
  }
};

const getOneOtp = async (query) => {
  try {
    const opt = await Otps.findOne(query).exec();
    return opt;
  } catch (err) {
    throw new Error(err.message);
  }
};

const compareOtp = async ({ userId, otp }) => {
  try {
    const userOpt = await Otps.findOne({
      userId,
    }).exec();

    if (otp !== userOpt?.otp) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'Otp is incorrect',
      }));
    }

    const dateNow = new Date();
    if (dateNow.getTime() > userOpt?.expirationTime.getTime()) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'Otp is expiration time',
      }));
    }

    return true;
  } catch (err) {
    throw new Error(err.message);
  }
};

const deleteMany = async ({ query, credentials }) => {
  try {
    const otps = await Otps.find(query).exec();

    if (!otps?.length) {
      return true;
    }

    const { validDataLength } = validateAccessToList({
      data: otps,
      credentials,
    });

    if (validDataLength !== otps.length) {
      throw new Error(JSON.stringify({
        code: 403,
        message: 'ValidationError',
      }));
    }

    await Otps.deleteMany(query).exec();

    return true;
  } catch (error) {
    return Promise.reject(error);
  }
};

export default {
  getOtps,
  createOneOtp,
  getOneOtp,
  compareOtp,
  deleteMany,
};
