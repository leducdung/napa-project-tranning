import to from '../../utils/to.js';
import service from './service.js';

const createOtp = async (req, res) => {
  try {
    const [error, data] = await to(service.createOneOtp(req.body));

    res.send({ error, data });
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  createOtp,
};
