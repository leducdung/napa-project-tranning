import { mongoose, Schema } from '../../utils/mongoose.js';

const otpSchema = new Schema(
  {
    userId: { type: Schema.Types.ObjectId, ref: 'Users' },
    otp: { type: String, default: null },
    expirationTime: { type: Date },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);

export default mongoose.model('otps', otpSchema);
