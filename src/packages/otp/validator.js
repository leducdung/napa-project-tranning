import Joi from 'joi';

export default {
  getOtp: {
    body: Joi.object({
      email: Joi.string().email().trim().required(),
    }),
  },
};
