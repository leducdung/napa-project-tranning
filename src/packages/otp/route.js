import express from 'express';
import controller from './controller.js';
import paramValidator from '../../middlewares/validator.js';

export const router = express.Router();
router.post(
  '/reset/password',
  paramValidator.otp.getOtp,
  controller.createOtp,
);

export default router;
