import sgMail from '@sendgrid/mail';
import configEnv from '../../env/index.js';

sgMail.setApiKey(configEnv.sendGrid.apiKey);
const getOTPEmailData = ({
  to, subject, text, typeEmail, otp,
}) => {
  const msg = {
    to,
    from: 'leducdung1409@gmail.com',
    subject,
    text,
    html: `<p>Your ${typeEmail} otp: <strong>${otp}</strong></p>`,
  };
  return msg;
};

const sendEmail = async (emailData) => {
  try {
    const result = await sgMail.send(emailData);

    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  sendEmail,
  getOTPEmailData,
};
