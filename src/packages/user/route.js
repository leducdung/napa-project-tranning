import express from 'express';
import controller from './controller.js';
import paramValidator from '../../middlewares/validator.js';
import authentication from '../../middlewares/authentication.js';
import authorization from '../../middlewares/authorizator.js';
import { roleName } from '../../common/enum.js';

export const router = express.Router();

/**
 * @swagger
 * /books/{id}:
 *   get:
 *     summary: Get the book by id
 *     tags: [Books]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The book id
 *     responses:
 *       200:
 *         description: The book description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Book'
 *       404:
 *         description: The book was not found
 */
router.get(
  '',
  authentication.authenticateToken,
  authorization.checkRoles([roleName.admin, roleName.basic]),
  controller.findManyUser,
);

/**
 * @swagger
 * /customers:
 *    put:
 *      description: Use to return all customers
 *    parameters:
 *      - name: customer
 *        in: query
 *        description: Name of our customer
 *        required: false
 *        schema:
 *          type: string
 *          format: string
 *    responses:
 *      '201':
 *        description: Successfully created user
 */
router.get(
  '/:userId',
  authentication.authenticateToken,
  authorization.checkRoles([roleName.admin, roleName.basic]),
  controller.findOneUser,
);

router.put(
  '/:userId',
  authentication.authenticateToken,
  authorization.checkRoles([roleName.admin, roleName.basic]),
  paramValidator.user.updateOneUser,
  controller.updateOne,
);

router.delete(
  '/:userId',
  authentication.authenticateToken,
  authorization.checkRoles([roleName.admin]),
  controller.deleteOneUser,
);

router.put(
  '/active/inactive/:userId',
  authentication.authenticateToken,
  authorization.checkRoles([roleName.admin]),
  paramValidator.user.activeOrInactiveUser,
  controller.activeOrInactiveUser,
);

router.post(
  '/register',
  paramValidator.user.register,
  controller.createUser,
);

router.post('/login', paramValidator.user.login, controller.login);
router.put(
  '/changePassword',
  authentication.authenticateToken,
  authorization.checkRoles([]),
  paramValidator.user.changePassword,
  controller.changePassword,
);

router.post(
  '/reset/password',
  paramValidator.user.resetPassword,
  controller.resetPassword,
);

router.get('/error/inactiveUser', controller.errorInactiveUser);

export default router;
