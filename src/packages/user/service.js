import bcrypt from 'bcrypt';
import { User } from '../../models.js';
import userAccessesService from '../userAccesses/service.js';
import { roleName, UserAccessStatus, UserStatus } from '../../common/enum.js';
import tokenGenerator from '../../utils/jwt.js';
import configEnv from '../../env/index.js';
import otpService from '../otp/service.js';
import { validateAccessToSingle, validateAccessToList } from '../../utils/validateAccesses.js';
import { buildFindingQuery } from '../../utils/build.js';

const getUsers = async () => {
  try {
    const users = await User.find({
      deletedAt: null,
    })
      .sort({ createdAt: -1 })
      .limit(5);

    return users;
  } catch (err) {
    throw new Error(err.message);
  }
};

const getManyUsers = async ({ query, credentials }) => {
  try {
    const options = buildFindingQuery(query);
    const {
      limit, skip, sort, search,
    } = options;
    const where = search;

    const [users, total] = await Promise.all([
      User.find(where)
        .sort(sort)
        .skip(skip)
        .limit(limit),
      User.countDocuments(where),
    ]);

    const { validData } = validateAccessToList({
      data: users,
      credentials,
    });

    return {
      list: validData,
      pagination: {
        total,
        limit,
        sort,
      },
    };
  } catch (err) {
    throw new Error(err.message);
  }
};

const registerByUserName = async (data) => {
  try {
    const user = await User.findOne({
      $or: [{ username: data.username }, { email: data.email }],
    }).exec();

    if (user) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'username or email already exists',
      }));
    }

    const salt = bcrypt.genSaltSync(configEnv.bcrypt.saltRounds);
    const hash = bcrypt.hashSync(data.password, salt);
    Object.assign(data, { password: hash });

    const createUser = await User.create(data);

    await userAccessesService.createOneAccess({
      userId: createUser._id,
      roleName: roleName.basic,
      status: UserAccessStatus.accepted,
    });

    const userResult = createUser.toObject();
    delete userResult.password;

    return userResult;
  } catch (err) {
    throw new Error(err.message);
  }
};

const login = async (query) => {
  try {
    const user = await User.findOne({
      username: query?.username,
    }).select('username').select('status').select('password')
      .exec();

    if (!user) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'Username is incorrect',
      }));
    }

    if (user.status === UserStatus.inactive) {
      throw new Error(JSON.stringify({
        code: 403,
        message: 'Inactive user',
      }));
    }

    const comparePassword = bcrypt.compareSync(query.password, user.password);

    if (!comparePassword) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'Password is incorrect',
      }));
    }

    const token = tokenGenerator.generate({ userId: user._id });
    return token;
  } catch (err) {
    throw new Error(err.message);
  }
};

const googleLogin = async (data) => {
  try {
    const userInfo = data._json;

    const user = await User.findOne({
      googleId: data?.id,
    });

    let token;
    if (user?.status === UserStatus.inactive) {
      throw new Error(JSON.stringify({
        code: 403,
        message: 'inactive user',
      }));
    }

    if (user?.status === UserStatus.active) {
      token = tokenGenerator.generate({ userId: user._id });
      return token;
    }

    const createOne = await User.create({
      googleId: userInfo.sub,
      firstName: userInfo.family_name,
      lastName: userInfo.given_name,
      email: userInfo.email,
      profilePhoto: userInfo.picture,
      status: UserStatus.active,
    });

    await userAccessesService.createOneAccess({
      userId: createOne._id,
      roleName: roleName.basic,
      status: UserAccessStatus.accepted,
    });

    token = tokenGenerator.generate({ userId: createOne._id });

    return token;
  } catch (err) {
    throw new Error(err.message);
  }
};

const gitHubLogin = async (userGithub) => {
  try {
    const userInfo = userGithub.data;

    const user = await User.findOne({
      githubId: userInfo?.id,
    }).exec();

    let token;
    if (user?.status === UserStatus.inactive) {
      throw new Error(JSON.stringify({
        code: 403,
        message: 'inactive user',
      }));
    }

    if (user?.status === UserStatus.active) {
      token = tokenGenerator.generate({ userId: user._id });
      return token;
    }

    const createOne = await User.create({
      githubId: userInfo.id,
      firstName: userInfo.login,
      lastName: userInfo.name,
      email: userInfo.email,
      profilePhoto: userInfo.avatar_url,
      status: UserStatus.active,
    });

    await userAccessesService.createOneAccess({
      userId: createOne._id,
      roleName: roleName.basic,
      status: UserAccessStatus.accepted,
    });

    token = tokenGenerator.generate({ userId: createOne._id });

    return token;
  } catch (err) {
    throw new Error(err.message);
  }
};

const findOne = async ({
  query, needToCheckExist = true, needToValidateAccess = true, credentials,
}) => {
  try {
    const user = await User.findOne(query).exec();

    if (!user && needToCheckExist) {
      throw new Error(JSON.stringify({
        code: 404,
        message: 'User Not Found',
      }));
    }

    if (needToValidateAccess) {
      validateAccessToSingle({
        data: user,
        credentials,
      });
    }

    return user;
  } catch (err) {
    throw new Error(err.message);
  }
};

const changePassword = async ({ password, newPassword, credentials }) => {
  try {
    const user = await User.findOne({
      _id: credentials.userId,
    }).select('username').select('password').exec();

    if (!user) {
      throw new Error(JSON.stringify({
        code: 404,
        message: 'User Not Found',
      }));
    }

    if (!user?.username || !user?.password) {
      throw new Error(JSON.stringify({
        code: 404,
        message: 'User Not login with userName',
      }));
    }

    const comparePassword = bcrypt.compareSync(password, user.password);

    if (!comparePassword) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'Password is incorrect.',
      }));
    }

    const salt = bcrypt.genSaltSync(configEnv.bcrypt.saltRounds);
    const hash = bcrypt.hashSync(newPassword, salt);

    Object.assign(user, { password: hash });
    await user.save();
    return true;
  } catch (err) {
    throw new Error(err.message);
  }
};

const resetPassword = async (resetPasswordData) => {
  try {
    const user = await User.findOne({
      email: resetPasswordData.email,
    }).select('username').select('password').exec();

    if (!user) {
      throw new Error(JSON.stringify({
        code: 404,
        message: 'User Not Found',
      }));
    }

    if (!user?.username || !user?.password) {
      throw new Error(JSON.stringify({
        code: 400,
        message: 'User Not login with userName',
      }));
    }

    await otpService.compareOtp({
      userId: user?._id,
      otp: resetPasswordData.otp,
    });

    const salt = bcrypt.genSaltSync(configEnv.bcrypt.saltRounds);
    const hash = bcrypt.hashSync(resetPasswordData.newPassword, salt);

    Object.assign(user, { password: hash });
    await user.save();
    return true;
  } catch (err) {
    throw new Error(err.message);
  }
};

const updateOne = async ({
  query, data, credentials,
}) => {
  try {
    const user = await User.findOne(query).exec();

    if (!user) {
      throw new Error(JSON.stringify({
        code: 404,
        message: 'User Not Found',
      }));
    }

    validateAccessToSingle({
      data: user,
      credentials,
    });

    Object.assign(user, data);

    await user.save();
    return user;
  } catch (err) {
    throw new Error(err.message);
  }
};

const deleteOne = async ({
  query, credentials,
}) => {
  try {
    const user = await User.findOne(query).exec();

    if (!user) {
      throw new Error(JSON.stringify({
        code: 404,
        message: 'User Not Found',
      }));
    }

    validateAccessToSingle({
      data: user,
      credentials,
    });

    await Promise.all([
      User.deleteOne(query).exec(),
      userAccessesService.deleteMany({
        query: {
          userId: user._id,
        },
        credentials,
      }),
      otpService.deleteMany({
        query: {
          userId: user._id,
        },
        credentials,
      }),
    ]);

    return true;
  } catch (err) {
    throw new Error(err.message);
  }
};

export default {
  getUsers,
  registerByUserName,
  login,
  googleLogin,
  findOne,
  gitHubLogin,
  changePassword,
  resetPassword,
  getManyUsers,
  updateOne,
  deleteOne,
};
