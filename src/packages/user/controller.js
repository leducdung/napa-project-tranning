import to from '../../utils/to.js';
import service from './service.js';
import responseController from '../../utils/responseController.js';

const getUser = async (req, res) => {
  try {
    res.send({
      a: 1,
    });
  } catch (error) {
    throw new Error(error);
  }
};

const createUser = async (req, res) => {
  try {
    const [error, data] = await to(service.registerByUserName({
      status: 'active',
      ...req.body,
    }));
    responseController({ error, data }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const login = async (req, res) => {
  try {
    const [error, token] = await to(service.login(req.body));

    responseController({ error, data: token }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const changePassword = async (req, res) => {
  try {
    const [error, updated] = await to(service.changePassword({
      password: req.body.password,
      newPassword: req.body.newPassword,
      credentials: req.userPayload,
    }));

    responseController({ error, data: updated }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const resetPassword = async (req, res) => {
  try {
    const [error, updated] = await to(service.resetPassword(req.body));

    responseController({ error, data: updated }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const errorInactiveUser = async (req, res) => {
  try {
    res.send({
      code: 403,
      message: 'inactive user',
    });
  } catch (error) {
    throw new Error(error);
  }
};

const findOneUser = async (req, res) => {
  try {
    const [error, user] = await to(service.findOne({
      query: {
        _id: req.params.userId,
      },
      credentials: req.userPayload,
    }));

    responseController({ error, data: user }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const findManyUser = async (req, res) => {
  try {
    const [error, user] = await to(service.getManyUsers({
      query: req.query,
      credentials: req.userPayload,
    }));

    responseController({ error, data: user }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const updateOne = async (req, res) => {
  try {
    const [error, user] = await to(service.updateOne({
      query: {
        _id: req.params.userId,
      },
      data: req.body,
      credentials: req.userPayload,
    }));

    responseController({ error, data: user }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const deleteOneUser = async (req, res) => {
  try {
    const [error, user] = await to(service.deleteOne({
      query: {
        _id: req.params.userId,
      },
      data: req.body,
      credentials: req.userPayload,
    }));

    responseController({ error, data: user }, res);
  } catch (error) {
    throw new Error(error);
  }
};

const activeOrInactiveUser = async (req, res) => {
  try {
    const [error, user] = await to(service.updateOne({
      query: {
        _id: req.params.userId,
      },
      data: req.body,
      credentials: req.userPayload,
    }));

    responseController({ error, data: user }, res);
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  getUser,
  createUser,
  login,
  errorInactiveUser,
  changePassword,
  resetPassword,
  findOneUser,
  findManyUser,
  updateOne,
  deleteOneUser,
  activeOrInactiveUser,
};
