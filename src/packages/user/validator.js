import Joi from 'joi';
import { UserStatus } from '../../common/enum.js';

export default {
  register: {
    body: Joi.object({
      username: Joi.string().required(),
      password: Joi.string()
        .min(8)
        .regex(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/)
        .message('Password must have at least 1 number, 1 uppercase and 1 special characters')
        .required(),
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().trim().required(),
    }),
  },
  login: {
    body: Joi.object({
      username: Joi.string()
        .required(),
      password: Joi.string()
        .min(8)
        .regex(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/)
        .message('Password must have at least 1 number, 1 uppercase and 1 special characters')
        .required(),
    }),
  },
  changePassword: {
    body: Joi.object({
      password: Joi.string()
        .min(8)
        .regex(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/)
        .message('Password must have at least 1 number, 1 uppercase and 1 special characters')
        .required(),
      newPassword: Joi.string()
        .min(8)
        .regex(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/)
        .message('Password must have at least 1 number, 1 uppercase and 1 special characters')
        .required(),
    }),
  },
  resetPassword: {
    body: Joi.object({
      otp: Joi.string().required(),
      newPassword: Joi.string()
        .min(8)
        .regex(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/)
        .message('Password must have at least 1 number, 1 uppercase and 1 special characters')
        .required(),
      email: Joi.string().email().trim().required(),
    }),
  },
  updateOneUser: {
    body: Joi.object({
      firstName: Joi.string().optional(),
      lastName: Joi.string().optional(),
    }),
  },
  activeOrInactiveUser: {
    body: Joi.object({
      status: Joi.string()
        .valid(...Object.values(UserStatus))
        .optional(),
    }),
  },
};
