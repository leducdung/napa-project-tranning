import { mongoose, Schema } from '../../utils/mongoose.js';

export const UserStatus = {
  inactive: 'inactive',
  active: 'active',
};
const UserSchema = new Schema(
  {
    username: {
      type: String,
    },
    status: {
      type: String,
      enum: [UserStatus.inactive, UserStatus.active],
      default: UserStatus.inactive,
    },
    profilePhoto: { type: String },
    email: { type: String },
    password: { type: String, unique: true, select: false },
    firstName: { type: String, default: null },
    lastName: { type: String, default: null },
    googleId: { type: String, default: null },
    githubId: { type: String, default: null },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);

export default mongoose.model('Users', UserSchema);
