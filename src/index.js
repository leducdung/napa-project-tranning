import express from 'express';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth20';
import swaggerJsDoc from 'swagger-jsdoc';
import { ValidationError } from 'express-validation';
import dotenv from 'dotenv';
import route from './routes/index.js';
import configEnv from './env/index.js';
import init from './init/index.js';

const bootstrap = async () => {
  const app = express();
  dotenv.config();
  const port = 3000;

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use('/', route());

  app.get('/customers', (req, res) => {
    res.status(200).send('Customer results');
  });

  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });

  passport.use(new GoogleStrategy(
    {
      clientID: configEnv.google.clientID,
      clientSecret: configEnv.google.clientSecret,
      callbackURL: configEnv.google.callbackURL,
    },
    ((accessToken, refreshToken, profile, done) =>
      // eslint-disable-next-line implicit-arrow-linebreak
      done(null, profile)
    ),
  ));

  app.use(passport.initialize());

  app.use((err, req, res, next) => {
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json(err);
    }

    return res.status(500).json(err);
  });

  await init(app);
  const swaggerOptions = {
    swaggerDefinition: {
      info: {
        version: '1.0.0',
        title: 'Customer API',
        description: 'Customer API Information',
        contact: {
          name: 'Amazing Developer',
        },
        servers: ['http://localhost:3000'],
      },
    },
    // ['.routes/*.js']
    apis: ['src/packages/*/route.js'],
  };
  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
    console.log(`Example app listening on port ${configEnv.nodeEnv}`);
    console.log(process.env.NODE_ENV);
  });
};

bootstrap();
