export default (promise) => {
  const getData = promise.then((data) => [null, data]).catch((error) => {
    try {
      return [JSON.parse(error.message)];
    } catch (err) {
      console.log('err when parsing json', err);
      return [error];
    }
  });

  return getData;
};
