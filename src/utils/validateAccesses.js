import { roleName } from '../common/enum.js';

export const validateAccessToSingle = ({ data, credentials }) => {
  if (!credentials) {
    throw new Error(JSON.stringify({
      code: 403,
      message: 'ValidationError',
    }));
  }

  const {
    isAdmin,
    userId,
    roles = [],
  } = credentials;

  if (!data || isAdmin || roles.includes(roleName.admin)) {
    return data;
  }

  const ownerIds = [data._id, data._id?.userId, data?.createdBy,
    data.createdBy?.userId, data?.userId?.userId, data.userId];
  const stringOwnerIds = ownerIds.map((ownerId) => {
    if (!ownerId) return;

    return ownerId.toString();
  });
  const isOwner = stringOwnerIds.includes(userId);

  if (isOwner) {
    return data;
  }

  throw new Error(JSON.stringify({
    code: 403,
    message: 'ValidationError',
  }));
};

export const validateAccessToList = ({ data, credentials }) => {
  if (!credentials) {
    throw new Error(JSON.stringify({
      code: 403,
      message: 'ValidationError',
    }));
  }

  const {
    isAdmin,
    userId,
    roles = [],
  } = credentials;

  if (!data?.length || isAdmin || roles.includes(roleName.admin)) {
    return {
      validData: data,
      validDataLength: data?.length || 0,
    };
  }

  let validDataLength = 0;

  const validData = data.map((each) => {
    const ownerIds = [each._id, each._id?.userId, each?.createdBy,
      each.createdBy?.userId, each?.userId?.userId, each.userId];
    const stringOwnerIds = ownerIds.map((ownerId) => {
      if (!ownerId) return;

      return ownerId.toString();
    });
    const isOwner = stringOwnerIds.includes(userId);

    if (isOwner) {
      validDataLength += 1;

      return each;
    }

    return null;
  });

  return {
    validData,
    validDataLength,
  };
};

export default {
  validateAccessToSingle,
  validateAccessToList,
};
