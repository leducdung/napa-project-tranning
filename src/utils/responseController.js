export default ({ error, data }, res) => {
  if (!error) {
    return res.send({ error, data });
  }

  return res.status(error.code).json(error);
};
